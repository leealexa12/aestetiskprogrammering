# Alexandra's MiniX6
Here is a link to my [program](http://leealexa12.gitlab.io/aestetiskprogrammering/miniX6/index.html)<br>
![](minix6.1.png) ![](minix6.2.png) ![](minix6.3.png)

[Click here to see my code](https://gitlab.com/leealexa12/aestetiskprogrammering/-/blob/main/miniX6/sketch.js)

## What did i create?
For this MiniX I have created a game that is inspired by the ToFu game. The purpose of the game is to get Pikachu to catch the cats. The player has 10 lives in the beginning of the game, and loses one life every time they miss catching a cat. The controls for my game are the up and down arrows which move the Pikachu up and down. When the player loses all 10 lives, then it's "GAME OVER".

### What did I learn while creating this MiniX?
The new syntaxes that I have used for this MiniX are the ones surrounding the cats, so the
`class Cat { constructor() { }`
 which defines the aspects of the cats in the game. To add the characters to my program, I have used the function
```
preload(){
    pikachu = loadImage();
```

which allowed me to use any gif as an element in my game. 
  <br> Another interesting syntax in my program is `function checkCatNum() { 
   ```
 if (cat.length < mini_cat) {
      cat.push(new Cat());
    }
```
This code highlights the way object-oriented programming works, the way it focuses on onjects and data (data that was already written under class, describing the object). 

Another code in my program that focuses on the object-oriented part on my game is 
```
function showCat(){
    for (let i = 0; i <cat.length; i++) {
      cat[i].move();
      cat[i].show();
    }
```
which is a loop that works with the different aspects such as `move()` and `show()` that are describing the objects under the `class Cat{}` section

 #### Reflection
In my perspective, the best games are the ones where the player can relax and not think so much about anything in particular, a form of escapism. For this MiniX i wanted to focus on doing exactly this, a game that anyone can play without much effort needed. I chose to focus on animated/non human characters to avoid as much as possible connecting the real world to my game. Although i though i succeseded in doing so thanks to the object-orientet programming (which i found very challenging and complex to understand in the beginning) where i focused on every detail of my cats through the syntax `class Cat{}`, we can still connect my game to real life experiences._"Object abstraction in computing is about representation. Certain attributes and relationships are abstracted from the real world, whilst simultaneously leaving details and contexts out." (Soon & Cox, 2020, p. 146)._ In computing, people will always be influenced by real life experiences, either while creating something, or while interpreting something. My game is based on a simple concept with easy rules: you lose a life every time you miss a cat, losing the game when you lost a total of 10 lives, something that is quite impossible in real life since we only have one life to lose. If we think about the cats in a more abstract context, they can be compared to the time that passes by, each lost cat representing 10 years of a human life, a human dying at about 100 years old. The cats can also be compared to the opportunities that we encounter in our everyday life and the preassure that we face to achieve all of them, Pikachu being a human trying his best to keep up with all the expectations comming towards him. There are a lot of real life concepts and objects that we can attribute to my non human characters in game that describe different aspects of our lives.

_Thank you for reading-Alex_ <br>

![](https://media4.giphy.com/media/fAD9SMlNWp0k84Ra1G/giphy.gif?cid=6c09b952fe8948ad109ccbb32c3e963d2ba134484311031d&rid=giphy.gif&ct=g)
