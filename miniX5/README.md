# Alexandra's MiniX5
Here is a link to my [program](http://leealexa12.gitlab.io/aestetiskprogrammering/miniX5/index.html)<br>
![](minix5.1.png) ![](minix5.2.png) ![](minix5.3.png)

[Click here to see my code](https://gitlab.com/leealexa12/aestetiskprogrammering/-/blob/main/miniX5/sketch.js)

## What did i create?
For this MiniX I have created a program that generates random lines and circles on the canvas. It is a program without an ending, which draws lines infinitely. The lines that are generated have random colors and lengths, being drawn from a random point on the canvas towards the right edge of the canvas, while the circles have random radius and are drawn on random points of the canvas. However, my program has a twist: it calculates if any of the circles overlap, if there is no overlapping, more circles are drawn on the canvas. There is no interaction in this MiniX

### What did I learn while creating this MiniX?
The new syntaxes that I have used for this MiniX are the loop syntaxes. The code `for (let i = 0; i < 100; i++) {
    let checkX = random(width);
    let checkY = random(height);
    let d = dist(x, y, checkX, checkY);
    if (d < radius) {
      overlapping = true;
      break;
    }` describes the loop in my program. The loop runs 100 times and checks the x and y coordinates of random points on the canvas. Furthermore it checks the distance between the center point on the canvas and the different circles drawn on the canvas. If the radius of the circles is bigger than the distance, then there is an overlapping which stops the loop. If the loop stops, the circle is not drawn. 
<br> In this MiniX I have also experimented with the code for `random()` which I've used for different parameters of my program such as the colors `random(255)` and placing `random(height)`. <br>Something new that I have learned while creating this program is the use of "!" in coding. An example where I've used it in my code is `if (!overlapping) {}`which translates to "if there is no overlapping, then...".

#### Reflection
Auto-generator in coding is a very helpful tool that was created to explore generative computer art. Auto-generators use algorithms and random number generators to create new content that is unpredictable, this is what we call generative computer art. <br>

Another important aspect of auto-generators is the sense of autonomy that they can give to artworks. In "Ten Questions Concerning Generative Computer Art," Jon McCormack et al. discuss how generative computer art is not simply about creating random patterns, but about using these techniques to explore new aesthetic possibilities. They highlight the work of Casey Reas, who uses auto-generators to create organic-looking patterns that evoke the natural world. By allowing the program to generate the content on its own, Reas creates a sense of autonomy in the artwork that is both fascinating and unsettling.

One of the biggest advantages of using auto-generator when creating art is the fact that it has the ability to create patterns that humans are not entirely able to reproduce without a machinery such as a computer, opening up for an entire new category of art, the computer generative art.

My program highlights the way coding can be used to create art. It would be practically impossible for a human being to draw my program since it draws lines infinitely. 

This week's reading made me think about another topic we've read about in Software studies, the way that technology can be used to create unique art. One example is the project 386DX created by Alexei Shulgin which uses computer to create a covers in russion of different famous songs such as Rape me by Nirvana.


_Thank you for reading-Alex_ <br>

![](https://media2.giphy.com/media/fThUjrYSsgU7TmslyB/giphy.gif?cid=6c09b952c0d347cbedddf47ce7ec38762b0c6732279418e2&rid=giphy.gif&ct=g)
