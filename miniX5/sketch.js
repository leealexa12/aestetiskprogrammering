function setup() {
  createCanvas(600, 600);
  background(229, 204, 255);
}

function draw() {
  //This code defines the lines on my program
  strokeWeight(random(1, 8)); //the weight of the lines is going to be a random one between 1 and 8
  stroke(random(255), random(255), random(255)); //the color of the lines is also going to be random
  line(random(width), random(height), width, random(height)); //The lines will be drawn from a random point on the canvas to the right edge of the canvas, at a random height.
  //defines the x and y coordinates for the center point on the canvas
  let x = random(width);
  let y = random(height);
  
  let radius = random(10, 80); //radius of the circles is going to be random between 10 and 80
  let overlapping = false; //in the beginning the overlapping is automatically set to false

  //This code describes the loop in my program. The loop runs 100 times and checks the x and y of random points on the canvas
  for (let i = 0; i < 100; i++) {
    let checkX = random(width);
    let checkY = random(height);
    let d = dist(x, y, checkX, checkY); //defines the distance between random points on the canvas and the center point
    if (d < radius) { //if the distance is smaller than the radius, overlapping becomes true and the loop breaks
      overlapping = true;
      break;
    }
  }
  if (!overlapping) { //if the there is no vorlapping, then random circles are drawn on the canvas with random colors
    noStroke();
    fill(random(255), random(255), random(255));
    ellipse(x, y, radius * 2);
  }
}



 
