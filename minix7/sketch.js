//code that defines different variables such as capture and loading bar, which is set at 0 from the beginning
let capture;
let loadingBar = 0;
let spaceBarPressed = false; //in the beginning it is defined that the space bar is not pressed

function setup() {
  //create a drawing canvas
  createCanvas(windowWidth, windowHeight);
  frameRate(12);
//code that defines the video capturing
  capture = createCapture(VIDEO);
  capture.size(400, 500);
  capture.hide();
}

function draw() {
  background(255, 153, 204, 30);
  if (spaceBarPressed) { //if the space bar is pressed, then the video capturing is shown
    image(capture, 20, 20, 500, 500);
  } else { //if the space bar is not pressed, the screen shows the throbber
    drawThrobber();
    if (loadingBar < 700) { // stop loading bar at 700 pixels
      rect(400, 600, loadingBar, 60);//code for drwing the loading bar
      loadingBar += 5;// code for showing the loading bar grows 5 pixels every frame
    }
  }
}

function drawThrobber() {
  //How many circles there are in my throbber
  let num = 10;
  push();
  //move things to the center
  translate(width / 2, height / 2);
  /* 360/num >> degree of each ellipse's movement;
  frameCount%num >> get the remainder that to know which one
  among 8 possible positions.*/
  let cir = 360 / num * (frameCount % num);
  rotate(radians(cir));
  //the code for the "Loading..." text
  textAlign(CENTER);
  textFont('Times New ROman');
  textSize(30);
  fill(50);
  text('Loading...', 10, 220);

  //the circles from the throbber
  noStroke();
  push();
  fill(255, 204, 153);
  circle(30, 70, 100);
  pop();
  pop();

  //This part of my code changes the background and text when the mouse is pressed
  if (mouseIsPressed) {
    background(192);
    textAlign(CENTER, CENTER);
    textFont('Times New Roman');
    textSize(100);
    fill(50);
    text('WARNING! Patience:LOW', windowWidth / 2, windowHeight / 2);
  }
  //This part of my code changes the background and the text when any key but the space bar is pressed
  if (keyIsPressed && !spaceBarPressed) {
    background(255, 0, 0);
    textAlign(CENTER, CENTER);
    textFont('Times New Roman');
    textSize(100);
    fill(50);
    text('Please wait, I am trying my best...', windowWidth / 2, windowHeight / 2);
  }
}
//when the space bar is pressed, the background changes
function keyPressed() {
  if (keyCode === 32) {
    spaceBarPressed = true;
    background(155, 204, 255);
  }
}
//when the space bar is released, spaceBarPressed turns automatically false, which changes my program to the initial screen, the throbber
function keyReleased() {
  if (keyCode === 32) {
    spaceBarPressed = false;
  }
}
//Thanks to this code, nothing changes when the window is resized
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}


