class Cat {
  constructor() { 
  this.speed = floor(random(4, 8));
  this.pos = new createVector(width+5, random(12, height/1.7));
  this.size = floor(random(20, 35));
  this.img=loadImage("data/catmeme.gif")
  }
move() {  
  this.pos.x-=this.speed;  
}
show() { //describe the cats
  push();
  translate(this.pos.x, this.pos.y);
  imageMode(CENTER);
  image(this.img, 0, 0, this.size, this.size);  
  pop();
}
}


