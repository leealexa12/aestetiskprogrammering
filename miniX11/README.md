# Alexandra's miniX11

## The group
For this MiniX I've worked together with Abishana and Sultan

[ReadMe](https://sultandd.gitlab.io/sltn/miniX11/index.html)

[The code-points](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX11/points.js)

[The code-game](https://gitlab.com/SultanDD/sltn/-/blob/main/miniX11/social.js)
