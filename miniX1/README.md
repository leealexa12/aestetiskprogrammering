# Alexandra's MiniX1
Welcome to my first MiniX. Please check out my RunMe first before reading my assignment [Click here for RunMe](http://leealexa12.gitlab.io/aestetiskprogrammering/miniX1/index.html)

Here you can see a screenshot of my program 
![](screenshot.png)

## What did i create?
For the first MiniX i chose to create a basic program where I have experimented with different geomethrical figures such as lines and circles. The program is drawing of an orange cat with triangular ears and big round eyes. The program doesn't have any moving elements or interactions that can engage the user, everything being a 2D element.

### Description of the working process
I've started my project by researching the p5.js reference list, mostly regarding the different geometrical shapes that i chose for my program. Something that really surprised me while creating my first project is how much math is behind coding. By choosing my canvas to be `createCanvas(500,500)` i knew from the beginning the parameters of my project. Now all i had to do is to calculate where the coordinates for the different elements are going to be on the canvas. On the p5.js reference list i found details about every shape and what each number from the code represents on the canvas. For instance `circle(50,100,20)` here 50=x coordinate for the center of the circle, 100=y coordinate for the center of the circle and 20=diameter of the circle

The most challenging shape to draw was the cat's head since i didn't want it to be a round circle. To draw the head i've used the `arc(250,250,440,450,0,PI)` <br>
For the top of the head i needed the same shape, but going the opposite way (upside down). Since i needed it to go the opposite way, i thought that i could use _-PI_. But it didn't work, since the minus isn't recognised by the computer. To finally get the shape to turn the opposite way i have changed the order of the numbers in my code,more specifically i've written 0 after PI. Example `arc(250,250,440,450,PI,0)`<br>

I would describe my first independent coding experince as a pleasant and also a bit chaotic experience at the same time. I did have a bit of experience with coding in the past when I have tried coding a homepage (HTML and CSS). Compared to my very first time coding, i found coding on Visual Studio Code a fun activity that allows us to express our ideas mainly though art thanks to p5js. I felt like a traveler using my map (the reference list) throughout my journey of creation. Since this is my first time writing java script code, one can easily observe the lack of professionalism as a result of the way the code is written, which can be described as chaotic. The lack of explanations/notes and spaces between the different paragraphs of code makes the code harder to read for one. 

#### Reflection 
Throughout my working process i've learned that the best way to learn coding is by actually practicing coding. One can get inspired by other people's codes, but it is crucial that the codes are understood before being put to use in a project. Programing is a very precise art, that needs a lot of attention. One spelling mistake or a misplaced dot can be fatal to the final product behind coding, compared to writing where usually one is able to understand the context of a text regardless of the spelling mistake. My lack of experience doesn't stop me from wishing to explore and learn more about coding.
##### How can i improve my code?
One obvious way to upgrade my program is to add some type of interaction to it such as moving shapes/elements. Another way that i could improve my program is by retouching the different parts of the cat such as the ears and the nose to make the drawing more visually appealing by adding more details such as more colors and shades.

