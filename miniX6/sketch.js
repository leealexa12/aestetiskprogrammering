let pikachuSize = { //This defines the size of Pikachu
    w:120,
    h:100,
  };
  //Here are defined different varibles such as the height of the playing area and the number of cats the game starts with
  let pikachu;
  let pikaPosY;
  let mini_height;
  let mini_cat = 5; 
  let cat = [];
  let score =0, lose = 0;
  let lives=10; //the player starts with 10 lives
  
  function preload(){ //preloads the pikachu gif
    pikachu = loadImage("data/pikachu.gif");
  }
  
  function setup() {
    createCanvas(800, 800);
    frameRate(30);
    pikaPosY = 800/2; //start position of pikachu
    mini_height = 800/2; //start position of cats
  }

  function draw() {
    background(76,0,153);
    displayScore(); //Displays the player's score
    checkCatNum(); //checks the number of cats displayed on the screen
    showCat();
    image(pikachu, 0, pikaPosY, pikachuSize.w, pikachuSize.h);
    checkEating(); //check is any cat was caught
    checkResult(); // displays the Game over message if the player loses 10 lives
  }
  
  function checkCatNum() { //checks if there are less than 5 cats on the screen and generates more
    if (cat.length < mini_cat) {
      cat.push(new Cat());
    }
  }
  
  function showCat(){//loops through the different cats while moving and generating cats
    for (let i = 0; i <cat.length; i++) {
      cat[i].move();
      cat[i].show();
    }
  }
  
  function checkEating() { //checks if pikachu has caught any cats
    for (let i = 0; i < cat.length; i++) {
      let d = int(
        dist(pikachuSize.w/2, pikaPosY+pikachuSize.h/2,
          cat[i].pos.x, cat[i].pos.y)
        );
      if (d < pikachuSize.w/2.5) { 
        score++; //updates the score
        cat.splice(i,1);
      }else if (cat[i].pos.x < 10) { 
        lose++;
        cat.splice(i,1);
        lives -= ceil(mini_cat / 10); //updates the number of lost cats and lost lives
      }
    }
  }
  
  function displayScore() { //code for the displayed score and its aestethics such as the placement and writing style
      fill(230);
      textSize(17);
      textStyle("Times new roman");
      text('Not bad, you have caught '+ score + " cats", 10, 800/1.4);
     // text('You have lost ' + lose + " cats", 10, 800/1.4+20);
      text("You have " + lives + " lives left", 10, 800 / 1.4 + 20);
      text('PRESS THE KEYS UP AND DOWN TO MOVE PIKACHU',
      800/4, 800/1.4+40);
  }
  
  function checkResult() { //Code for the game over display
    if (lose > score && lose>=10) {//if the player loses more cats than they have caught and have lost 10 lives, the Game over is shown and the loop is ended
      fill(255,255,0);
      textSize(40);
      text("GAME OVER", 800/3, 800/1.4);
      noLoop();
    }
  }
  //code for moving pikachu by using the arrows up and down
  function keyPressed() {
    if (keyCode === UP_ARROW) {
      pikaPosY-=25;
    } else if (keyCode === DOWN_ARROW) {
      pikaPosY+=25;
    }
    //Code that ensures that Pikachu does not leave the screen
    if (pikaPosY > mini_height) {
      pikaPosY = mini_height;
    } else if (pikaPosY < 0 - pikachuSize.w/2) {
      pikaPosY = 0;
    }
    return false;
  }
  
  