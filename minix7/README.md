# Alexandra's MiniX7
Here is a link to my [program](http://leealexa12.gitlab.io/aestetiskprogrammering/minix7/index.html)<br>
![](minix7.1.png) ![](minix7.2.png) ![](minix7.3.png) ![](minix7.4.png)

[Click here to see my code](https://gitlab.com/leealexa12/aestetiskprogrammering/-/blob/main/minix7/sketch.js)


## What did i create?
For this MiniX I've chose to work with the throbber from MiniX3. My inspiration was the subject about Data capturing. I wanted to incorporate capturing video in one of my  projects, since i found it challenging previously, not being able to make it work. My initial thoughts were that the throbber would be the perfect project to connect to data capturing. For this MiniX I've changed a bit the `keyIsPressed` codes and I've also added a loading bar at the bottom of the screen and, of course the video capturing function.

### What did I learn while creating this MiniX?
The new syntaxes that I have used for this MiniX are, as mentioned previously the ones that describe the video capturing ` image(capture, 20, 20, 500, 500);`. I found it quite challenging to use in my previous projects, but I'm happy I finally made it work. One challenge I've encountered while working on this program is getting the screen to go from the screen showing the video capturing back to the main screen showing the throbber. I have managed to do that by using the syntaxes `function keyPressed() {
  if (keyCode === 32) {
    spaceBarPressed = true;}` and `function keyReleased() {
  if (keyCode === 32) {
    spaceBarPressed = false;
  }`. Another new detail added to my previous MiniX is the loading bar, which was added using the syntax ` if (loadingBar < 700) { 
      rect(400, 600, loadingBar, 60);
      loadingBar += 5;
    }` which draws the loading bar while also definig the fact that it grows 5 pixels/frame while also stopping at 700 pixels.

### Reflection 
For this MiniX I have experienced, as mentioned previously, with video capturing through coding. Some concepts that are relevant mentioning for this MInix are data capturing and of course temporality thanks to the loading bar and the throbber. <br>

Temporality refers to the concept of time and the order in which events occur in within a program. As I also mentioned in my [MiniX3](https://gitlab.com/leealexa12/aestetiskprogrammering/-/tree/main/miniX3) a throbber indicates the fact that the program is working. Since throbbers can influence people's perception of time, making it seem like the time is slowing down I have decided to add a loading bar to my program, improving the user experience. A loading bar is an element used in software interfaces to provide feedback to the user regarding the process of a task. In this way, a loading bar is quite simmilar to a throbber, informing the user about a form of progress in the program. But I've chose to incorporate a loading bar to my program as a contrast to the throbber, the visual element of a loading bar playing a role in managing temporality in code, reducing the perception of time for the users by visually representing the aspect loading the task. <br>

Data capturing plays a big role in today's world due to the digitalization implemented and developed throughout time that is present in our day-to-day lives. The collection and storage of vast amounts of data from various sources is used to fuel algorithms, insights and desicion-making systems that dominate our digital world. Data collection "hides" behind 


_Thank you for reading-Alex_ <br>

![](https://media.tenor.com/uDlyUChUr4EAAAAM/punkdoe-flygohr.gif)
