# Alexandra's MiniX4
Here is a link to my [program](http://leealexa12.gitlab.io/aestetiskprogrammering/miniX4/index.html)<br>
![](minix4.1.png) ![](minix4.2.png) ![](minix4.3.png)

[Click here to see my code](https://gitlab.com/leealexa12/aestetiskprogrammering/-/blob/main/miniX4/sketch.js)

## What did i create?
For this MiniX I have created a program that resembles the familiar page "Terms and conditions" that one encounters in the digital world. When the program opens, one is  met with a grey screen with the text "Terms and conditions" at the top of the screen, a green circle in the middle and two buttons at the bottom of the screen: Agree and Cancel buttons. When one presses the Agree button, the background changes to a more warmer color, the circle changes to a neutral emoji and the "Thank you for your data" text appears on the screen. When one presses the Cancel button, the screen changes to a white screen and one is able to see the "Error 404" text.

### What did I learn while creating this MiniX?
The new syntaxes that I have used for this MiniX are the button syntaxes, so `buton=createButton("Agree")`and everything that has to do with the aesthetics of a button. While coding for the aesthetics, I have used the CSS language to set the different details such as the color of the button and the writing style inside the buttons. One example here being `buton.style("font-family", "Times New Roman")` which sets the text font to Times New Roman. I have also experienced and learned how to describe and use the different functions of a button, one example being `function ThankYou()` which changes the main screen of my program to another screen. One challenge that i have encountered in my program is setting more text on the main screen of the program. It is a basic code for inserting text `textString(blablabla, 250, 50);` I have explained a bit the problem directly in my RunMe. For some unknown reason the text doesn't want to show up on my program no matter what, but when I try to delete that part of coding from my RunMe (because it is basically not useful anymore since the text doesn't show up on the program), nothing works anymore the way it should. The fact that I wasn't able to make the text visible has inspired me to see my project from another perspective which i will discuss in the next paragraph. 

**Update**: I found out that textString is basically a variable, that is why I couldn't get the text to show up in my program. I still don't know why i can't delete that part from my code without my program crashing.
#### The transmediale open call “Capture All”
**Invisible terms and conditions**

Every digital product uses terms and conditions, usually to inform the users about a variety of details regarding the usage of the product. One important detail that is written about in the terms and conditions is about the users' data, what the product gets access to and how the collected data is going to be used. Terms and conditions are usually crucial to read and understand before agreeing to them, but the majority of people skip the reading part and agree directly to the different terms and conditions without really thinking about the possible consequences. My program, "Invisible terms and conditions" can be used as a metaphor for the way people perceive terms and conditions. Since we are not used to reading the different details of terms and conditions, we might as well agree to a blank screen. It also highlights how easy we agree to have our data collected and used in the digital world, which is also the focus of the "Capture All"  program. 

_Thank you for reading-Alex_

![](https://media.tenor.com/xaBwfY2otmkAAAAC/youve-agreed-to-all-of-this-kyle-broflovski.gif)
