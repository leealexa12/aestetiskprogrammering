
function setup() {
  //create a drawing canvas
  createCanvas(windowWidth, windowHeight);
  frameRate (12);
 }
 
 function draw() {
   background(255,153,204, 30);
   drawThrobber();
 }
 
 function drawThrobber() {
  //How many circles there are in my throbber
   let num = 10;
   push();
   //move things to the center
   translate(width/2, height/2);
   /* 360/num >> degree of each ellipse's movement;
   frameCount%num >> get the remainder that to know which one
   among 8 possible positions.*/
   let cir = 360/num*(frameCount%num);
   rotate(radians(cir));
   //the code for the "Loading..." text
   textAlign(CENTER);
 textFont('Times New ROman');
 textSize(30);
 fill(50);
 text('Loading...', 10, 220);

 //the circles from the throbber
   noStroke();
   push();
   fill(255,204,153);
   circle(30,70,100);
   pop();
   pop();

   //This part of my code changes the background and text when the mouse is pressed
   if(mouseIsPressed){
    background(192);
    textAlign(CENTER, CENTER);
 textFont('Times New Roman');
 textSize(100);
 fill(50);
 text('WARNING! Patience:LOW',windowWidth/2, windowHeight/2);
   }
   //This part of my code changes the background and the text when any key is pressed
   if(keyIsPressed){
    background(255,0,0);
    textAlign(CENTER, CENTER);
 textFont('Times New Roman');
 textSize(100);
 fill(50);
 text('Please wait, I am trying my best...',windowWidth/2, windowHeight/2);
   }
   
  
 }
 //Thanks to this code, nothing changes when the window is resized
 function windowResized() {
   resizeCanvas(windowWidth, windowHeight);
 }
 