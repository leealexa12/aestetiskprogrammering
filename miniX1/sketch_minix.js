function setup() {
  // put setup code here
  createCanvas(500,500);
  background(255, 51, 97);
}

function draw() {
  // put drawing code here
  smooth();
  fill(255, 202, 51);
  arc(250,250,440,450,0,PI);
  arc(250,250,250,120,PI,0);
  triangle(30,250,80,120,130,250
    );
    triangle(470,250,420,120,370,250);
    fill(255,255,255);
    circle(150,300,50);
    circle(350,300,50);
    fill(225, 66, 160);
    triangle(250,350,220,370,280,370);
    fill(100,171,56);
    circle(150,300,20);
    circle(350,300,20);
    fill(0,0,0);
    circle(150,300,8);
    circle(350,300,8);
    line(200,390,20,320);
    line(200,390,20,380);
    line(200,390,20,450);
    line(300,390,480,320);
    line(300,390,480,380);
    line(300,390,480,450);
    ellipse(250,400,60,20);
    
    
}
