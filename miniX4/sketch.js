let buton;
let knap; //the 2 variables for the buttons

function setup() {
  // put setup code here
  createCanvas(500,500);
  background(192);
  //codes for the button "Agree", the style codes change both the text of the button and also the way the button looks, such as the colors and the round corners.
  //for the style it is used the CSS language, such as #000000 describing the color
  buton=createButton("Agree");
  buton.position(50,350);
  buton.size(120,80);
  buton.style("background-color","#CCE5FF");
  buton.style("color","#000000");
  buton.style("border-radius","15px");
  buton.style("font-size","30px");
  buton.style("font-family", "Times New Roman")

//codes for the button Cancel", the style codes change both the text of the button and also the way the button looks, such as the colors and the round corners.
  knap=createButton("Cancel");
  knap.position(300,350);
  knap.size(120,80);
  knap.style("background-color","#CCE5FF");
  knap.style("color","#000000");
  knap.style("border-radius","15px");
  knap.style("font-size","30px");
  knap.style("font-family", "Times New Roman");

//when the mouse presses the "Agree" button, the function "thank you" starts
  buton.mousePressed(ThankYou);
  //when the mouse is not pressed anymore, the function "comeback" starts
  buton.mouseOut(ComeBack);
  
 //when the mouse presses the "Cancel" button, the function "ErrorShow" starts 
  knap.mousePressed(ErrorShow);
  //when the mouse is not pressed anymore, the function "comeback" starts
  knap.mouseOut(ComeBack)
}

function draw() {
  // this code is for the "Terms and conditions" text that one can see when the program starts
  textAlign(CENTER);
 textFont('Times New ROman');
 textSize(30);
 fill(50);
 text('Terms & conditions', 250, 30)

 fill(153,255,153);
 circle(250,250,200);
//this part of the code does not work. It was supposed to be the textfor Terms and conditions, but for some reason it doesn't show up.
// But i can't delete it either, i tried to delete it and everything in my program crashes. So basicallyu this part is useless for the program.
 textAlign(CENTER);
 textFont('Times New ROman');
 textSize(30);
 fill(50);
 textWrap(WORD);
 textString('Please agree to the terms and conditions of our website to be able to proceed to the information available on our website. By agreeing to our terms and conditions, you give us access to your data such as your location, IP adress, card information and your microphone. We hope you enjoy your stay and do not worry, your data is safe with us', 250, 50);
}

//this code describes the "ThankYou" function, which changes the background and allows for the other elements such as the text "Thank you for your data" and the emoji to be seen.

function ThankYou(){
  //The code for the text
background(255,153,153);
textAlign(CENTER);
 textFont('Times New ROman');
 textSize(30);
 fill(50);
 text('Thank you for your data', 250, 30);
//The code for the emoji
 fill(153,255,153);
 circle(250,250,200);
circle(210,210,30);
circle(290,210,30);
line(210,300,290,300);

}
//this code describes the "ComeBack" function, which changes the background and allows for the other elements such as the text "Terms and conditions" and the emoji to be seen.

function ComeBack(){
background(192);
textAlign(CENTER);
 textFont('Times New ROman');
 textSize(30);
 fill(50);
 text('Terms & conditions', 250, 30);
 fill(153,255,153);
 circle(250,250,200);

}
//this code describes the "ErrorShow" function, which changes the background and allows for the other elements such as the text "Error 404" to be seen.

function ErrorShow(){
  background(255);
  textAlign(CENTER);
   textFont('Times New ROman');
   textSize(30);
   fill(50);
   text('ERROR 404', 250, 250);
}

